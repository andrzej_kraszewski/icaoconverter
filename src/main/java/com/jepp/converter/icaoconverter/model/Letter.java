package com.jepp.converter.icaoconverter.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Letter {

    @Id
    private Long id;

    @Column(unique = true)
    private String letter;

    private String icaoCode;
    private String foneticCode;
}
