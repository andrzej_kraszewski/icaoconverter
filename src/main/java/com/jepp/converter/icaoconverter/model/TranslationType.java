package com.jepp.converter.icaoconverter.model;

public enum TranslationType {
    ICAO,
    FONETIC
}
