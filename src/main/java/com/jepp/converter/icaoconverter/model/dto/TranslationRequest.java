package com.jepp.converter.icaoconverter.model.dto;

import com.jepp.converter.icaoconverter.model.TranslationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TranslationRequest {

    private String yourText;
    private TranslationType type;
}
