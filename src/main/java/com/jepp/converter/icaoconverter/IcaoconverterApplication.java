package com.jepp.converter.icaoconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcaoconverterApplication {

    public static void main(String[] args) {
        SpringApplication.run(IcaoconverterApplication.class, args);
    }

}

