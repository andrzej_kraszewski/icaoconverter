package com.jepp.converter.icaoconverter.service;

import com.jepp.converter.icaoconverter.model.Words;
import com.jepp.converter.icaoconverter.repository.WordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class WordsService {

    @Autowired
    private WordsRepository wordsRepository;

/*    public void getLasttenRecordsByDate() {
        wordsRepository.findTopByQueryTime_Max(LocalDateTime.now());
    }*/

    public void saveWord(Words wordToAdd) {
        wordsRepository.save(wordToAdd);
    }
}
