package com.jepp.converter.icaoconverter.service;

import com.jepp.converter.icaoconverter.model.Letter;
import com.jepp.converter.icaoconverter.model.TranslationType;
import com.jepp.converter.icaoconverter.model.dto.TranslationRequest;
import com.jepp.converter.icaoconverter.repository.LetterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class IcaoConverter {

    @Autowired
    private LetterRepository letterRepository;

    public List<String> convertGivenWord(TranslationRequest request) {
        String yourText = request.getYourText();

        if (yourText.length() > 0) {

            String[] lettersToConvert = yourText.split("");
            List<String> letters = new ArrayList<>(Arrays.asList(lettersToConvert));

            letters = letters.stream()
                    .map(letter -> {
                        Optional<Letter> letterOptional = letterRepository.findByLetter(letter);
                        if (letterOptional.isPresent()) {
                            Letter ltr = letterOptional.get();

                            if (request.getType().equals(TranslationType.FONETIC)) {
                                return ltr.getFoneticCode();
                            } else {
                                return ltr.getIcaoCode();
                            }
                        }
                        return letter;
                    }).collect(Collectors.toList());

            return letters;
        }
        return new ArrayList<>();
    }
}
