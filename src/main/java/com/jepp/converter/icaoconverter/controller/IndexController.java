package com.jepp.converter.icaoconverter.controller;

import com.jepp.converter.icaoconverter.model.Words;
import com.jepp.converter.icaoconverter.model.dto.TranslationRequest;
import com.jepp.converter.icaoconverter.service.IcaoConverter;
import com.jepp.converter.icaoconverter.service.LetterService;
import com.jepp.converter.icaoconverter.service.WordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class IndexController {
    @Autowired
    private IcaoConverter converter;

    @Autowired
    private LetterService letterService;

    @Autowired
    private WordsService wordsService;

    @GetMapping("/index")
    public String getIndexPage() {
        return "index";
    }

    @PostMapping("/index")
    public String submitIcaoCode(TranslationRequest request, Model model) {

        Words text = new Words();
        text.setWord(request.getYourText());
        text.setQueryTime(LocalDateTime.now());
        wordsService.saveWord(text);

        List<String> word = converter.convertGivenWord(request);
        model.addAttribute("result", word);

        return "index";
    }
}
