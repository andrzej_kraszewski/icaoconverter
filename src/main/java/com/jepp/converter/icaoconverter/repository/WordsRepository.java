package com.jepp.converter.icaoconverter.repository;

import com.jepp.converter.icaoconverter.model.Words;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface WordsRepository extends JpaRepository<Words, Long> {

    //List<Words> findTopByQueryTime_Max(LocalDateTime dateTime);
}
