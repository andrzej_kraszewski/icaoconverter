package com.jepp.converter.icaoconverter;

import com.jepp.converter.icaoconverter.model.TranslationType;
import com.jepp.converter.icaoconverter.model.dto.TranslationRequest;
import com.jepp.converter.icaoconverter.service.IcaoConverter;
import com.jepp.converter.icaoconverter.service.LetterService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IcaoconverterApplicationTests {

    @Autowired
    private IcaoConverter icaoConverter;

    @Test
    public void willReturnNumberOfChars() {

        List<String> list = icaoConverter.convertGivenWord(new TranslationRequest("abc olaboga", TranslationType.ICAO));
        Assert.assertEquals(11, list.size());
        System.out.println(list);
    }
}

